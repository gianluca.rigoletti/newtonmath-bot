const TeleBot = require("telebot")
const outdent = require("outdent")
const fetch = require("node-fetch")
require("dotenv").config()
const bot = new TeleBot(process.env.TOKEN)
const _ = require("lodash")
const baseUrl = "https://newton.now.sh"
const operations = [
  "simplify",
  "derive",
  "factor",
  "integrate",
  "zeroes",
  "tangent",
  "area",
  "cosine",
  "sine",
  "tangent",
  "arccos",
  "arcsin",
  "arctan",
  "abs",
  "log"
]

bot.on("/start", msg => {
  const text = outdent`
    To list all the commands type /list
    If you need help on a command type /help <command>
    You can also use the bot inline by typing the expression
    in the form @newtonmath_bot <operation> <expression>
  `
  return bot.sendMessage(msg.chat.id, text)
})

bot.on("/list", msg => msg.reply.text(operations.join("\n")))

bot.on(/^(\w+)(\s+)(.+)$/, (msg, props) => {
  const operation = props.match[1].trim().toLowerCase()
  const expression = props.match[3].trim().replace(" ", "")
  const fullUrl = `${baseUrl}/${operation}/${encodeURIComponent(expression)}`
  console.log(fullUrl)
  fetch(fullUrl)
    .then(response => response.json())
    .then(response => {
      console.log(response)
      bot.sendMessage(msg.chat.id, response.result)
    })
    .catch(err => {
      bot.sendMessage(msg.chat.id, `Wrong typed message or network error`)
    })
})

bot.on(/^\/help simplify/, (msg, props) => {
  const reply = outdent`
    Simplify or evaluate an expression.
    Example:
    \`simplify 2^2+2(2)\`
    Result:
    \`8\`
  `
  return bot.sendMessage(msg.chat.id, reply, {
    parseMode: "Markdown"
  })
})

bot.on(/^\/help factor/, (msg, props) => {
  const reply = outdent`
    Factor out a symbolical expression
    Example:
    \`factor x^2 + 2x\`
    Result:
    \`x (x + 2)\`
  `
  return bot.sendMessage(msg.chat.id, reply, {
    parseMode: "Markdown"
  })
})

bot.on(/^\/help derive/, (msg, props) => {
  const reply = outdent`
    Derive a symbolical expression
    Example:
    \`derive x^2 + 2x\`
    Result:
    \`x^2+2x\`
  `
  return bot.sendMessage(msg.chat.id, reply, {
    parseMode: "Markdown"
  })
})

bot.on(/^\/help integrate/, (msg, props) => {
  const reply = outdent`
    Integrate a symbolical expression
    Example:
    \`integrate x^2+2x\`
    Result:
    \`1/3 x^3 + x^2 + C\`
  `
  return bot.sendMessage(msg.chat.id, reply, {
    parseMode: "Markdown"
  })
})

bot.on(/^\/help zeroes/, (msg, props) => {
  const reply = outdent`
    Find 0's of a function
    Example:
    \`zeroes x^2+2x\`
    Result:
    \`[2,0]\`
  `
  return bot.sendMessage(msg.chat.id, reply, {
    parseMode: "Markdown"
  })
})

bot.on(/^\/help tangent/, (msg, props) => {
  const reply = outdent`
    Find tangent of a function f in a point c by
    writing c|f(x)
    Example:
    \`tangent 2|x^3\`
    Result:
    \`12 x + -16\`
  `
  return bot.sendMessage(msg.chat.id, reply, {
    parseMode: "Markdown"
  })
})

bot.on(/^\/help area/, (msg, props) => {
  const reply = outdent`
    Compute area under a function f in the interval [a, b]
    by writing a:b|f(x)
    Example:
    \`area 2:4lx^3\`
    Result:
    \`60\`
  `
  return bot.sendMessage(msg.chat.id, reply, {
    parseMode: "Markdown"
  })
})

bot.on(/^\/help cos/, (msg, props) => {
  const reply = outdent`
    Compute the numerical cosine of a value
    Example:
    \`cos pi\`
    Result:
    \`-1\`
  `
  return bot.sendMessage(msg.chat.id, reply, {
    parseMode: "Markdown"
  })
})

bot.on(/^\/help sin/, (msg, props) => {
  const reply = outdent`
    Compute the numerical sine of a value
    Example:
    \`sin pi\`
    Result:
    \`0\`
  `
  return bot.sendMessage(msg.chat.id, reply, {
    parseMode: "Markdown"
  })
})

bot.on(/^\/help tan/, (msg, props) => {
  const reply = outdent`
    Compute the numerical tangent of a value
    Example:
    \`tan 0\`
    Result:
    \`0\`
  `
  return bot.sendMessage(msg.chat.id, reply, {
    parseMode: "Markdown"
  })
})

bot.on(/^\/help arccos/, (msg, props) => {
  const reply = outdent`
    Compute the inverse cosine of a value
    Example:
    \`arccos 1\`
    Result:
    \`0\` 
  `
  return bot.sendMessage(msg.chat.id, reply, {
    parseMode: "Markdown"
  })
})

bot.on(/^\/help arcsin/, (msg, props) => {
  const reply = outdent`
    Compute the inverse sine of a value
    Example:
    \`arccos 0\`
    Result:
    \`0\` 
  `
  return bot.sendMessage(msg.chat.id, reply, {
    parseMode: "Markdown"
  })
})

bot.on(/^\/help arcsin/, (msg, props) => {
  const reply = outdent`
    Compute the inverse tangent of a value
    Example:
    \`arctan 0\`
    Result:
    \`0\` 
  `
  return bot.sendMessage(msg.chat.id, reply, {
    parseMode: "Markdown"
  })
})

bot.on(/^\/help abs/, (msg, props) => {
  const reply = outdent`
    Compute the absolute value
    Example:
    \`abs -1\`
    Result:
    \`0\` 
  `
  return bot.sendMessage(msg.chat.id, reply, {
    parseMode: "Markdown"
  })
})

bot.on(/^\/help log/, (msg, props) => {
  const reply = outdent`
    Compute the logarithm of x in the base b by writing
    b|x
    Example:
    \`log 2|8\`
    Result:
    \`3\` 
  `
  return bot.sendMessage(msg.chat.id, reply, {
    parseMode: "Markdown"
  })
})

bot.on("inlineQuery", msg => {
  const regexp = /^(\w+)(\s+)(.+)$/
  const query = msg.query.trim().toLowerCase()
  const results = regexp.exec(query)
  const operation = results[1].trim()
  const expression = results[3].trim()
  const answers = bot.answerList(msg.id, { cacheTime: 60 })
  const fullUrl = `${baseUrl}/${operation}/${encodeURIComponent(expression)}`
  console.log(operation)
  console.log(expression)
  console.log(fullUrl)
  fetch(fullUrl)
    .then(response => response.json())
    .then(response => {
      answers.addArticle({
        id: "query",
        title: response.result,
        description: response.result,
        disable_web_page_preview: true,
        parse_mode: "Markdown",
        message_text: outdent`
          ${operation} ${expression}   ️
          *result*: ${response.result}
        `
      })
    })
    .catch(err => {
      answers.addArticle({
        id: "query",
        title: "No results available",
        description: "No results available",
        disable_web_page_preview: true,
        message_text: "No results available"
      })
    })
    .then(() => {
      bot.answerQuery(answers)
    })
})

process.on("unhandledRejection", (reason, p) => {
  console.log("Unhandled Rejection at: Promise", p, "reason:", reason)
})

bot.start()
