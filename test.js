const _ = require("lodash")

const commands = [
  "Simplify",
  "Derive",
  "Factor",
  "Integrate",
  "Find 0s",
  "Find Tangent",
  "Area Under Curve",
  "Cosine",
  "Sine",
  "Tangent",
  "Inverse Cosine",
  "Inverse Sine",
  "Inverse Tangent",
  "Absolute Value",
  "Logarithm"
]

console.log(_.chunk(commands, 3).unshift(["ciao"]))
